import 'package:flutter/material.dart';

class AppColor {

  static const Color gardenGreen = Color(0xFF0C9869);
  static const Color kindaBlack = Color(0xFF3C4046);
  static const Color lessWhite = Color(0xFFFCFCFC);
  static const Color navBarIcons = Color(0xFFCFCFCF);
  static const Color heading = Color(0xFF3C4046);
}