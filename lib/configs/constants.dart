import 'package:indoor_plants/model/big_card.dart';
import 'package:indoor_plants/model/small_card.dart';

List<SmallCardModel> smallCards = [
  SmallCardModel(
    imageAddress: 'assets/images/plant-01.png',
    plantName: 'Samantha',
    plantType: 'Indoor',
    cost: '\$400',
  ),
  SmallCardModel(
    imageAddress: 'assets/images/plant-02.png',
    plantName: 'Amanda',
    plantType: 'Indoor',
    cost: '\$200',
  ),
  SmallCardModel(
    imageAddress: 'assets/images/plant-03.png',
    plantName: 'Meriam',
    plantType: 'Indoor',
    cost: '\$340',
  ),
  SmallCardModel(
    imageAddress: 'assets/images/plant-04.png',
    plantName: 'Hanabi',
    plantType: 'Indoor',
    cost: '\$890',
  ),
  SmallCardModel(
    imageAddress: 'assets/images/plant-05.png',
    plantName: 'Miya',
    plantType: 'Indoor',
    cost: '\$400',
  ),
];

List<BigCardModel> bigCards = [
  BigCardModel(
    imageAddress: 'assets/images/plant-06.png',
    plantName: 'AMELIA',
    plantType: 'INDOOR',
    cost: '\$770',
  ),
  BigCardModel(
    imageAddress: 'assets/images/plant-07.png',
    plantName: 'JASMINE',
    plantType: 'INDOOR',
    cost: '\$830',
  ),
  BigCardModel(
    imageAddress: 'assets/images/plant-08.png',
    plantName: 'KATHARINA',
    plantType: 'INDOOR',
    cost: '\$400',
  ),
  BigCardModel(
    imageAddress: 'assets/images/plant-09.png',
    plantName: 'JESSICA',
    plantType: 'INDOOR',
    cost: '\$560',
  ),
];