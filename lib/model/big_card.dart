class BigCardModel {
  String imageAddress;
  String plantName;
  String plantType;
  String cost;

  BigCardModel({this.imageAddress, this.plantName, this.plantType, this.cost});
}