class SmallCardModel {
  String imageAddress;
  String plantName;
  String plantType;
  String cost;

  SmallCardModel({this.imageAddress, this.plantName, this.plantType, this.cost});
}