import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:indoor_plants/screens/homepage.dart';
import 'package:indoor_plants/screens/product_page.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light,
    ),
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
	DeviceOrientation.portraitUp,
]);

    return MaterialApp(
      title: 'Indoor Plants',
      home: Garden(),
     //home: ProductPage(),
    );
  }
}
