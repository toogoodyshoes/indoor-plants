import 'package:flutter/material.dart';
import 'package:indoor_plants/components/appbar.dart';
import 'package:indoor_plants/components/bot_nav_bar.dart';
import 'package:indoor_plants/components/tiles_big.dart';
import 'package:indoor_plants/components/tiles_small.dart';

class Garden extends StatefulWidget {
  @override
  _GardenState createState() => _GardenState();
}

class _GardenState extends State<Garden> {
  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    print(MediaQuery.of(context).textScaleFactor);

    return Stack(
      children: <Widget>[
        Scaffold(
          backgroundColor: Colors.white,
          body: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: _height * 0.35 + 20),
                child: CarouselOne(),
              ),
              Padding(
                 padding: EdgeInsets.only(top: 10.0),
                 child: CarouselTwo(),
              ),
              SizedBox(height: 50.0),
            ],
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            GreenAppBar(),
            WhiteBotNavBar(),
          ],
        ),
      ],
    );
  }
}
