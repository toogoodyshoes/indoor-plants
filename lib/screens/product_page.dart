import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:indoor_plants/components/rounded_rec.dart';
import 'package:indoor_plants/configs/colors.dart';
import 'package:indoor_plants/configs/constants.dart';

class ProductPage extends StatefulWidget {
  final int index;

  ProductPage({this.index});

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Stack(
      children: <Widget>[
        //* background >>>---------------------->
        Scaffold(
          backgroundColor: Colors.white,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      PlantInfoBox(icon: Feather.sun),
                      PlantInfoBox(icon: FontAwesome.thermometer_3),
                      PlantInfoBox(icon: MaterialCommunityIcons.water_outline),
                      PlantInfoBox(icon: MaterialCommunityIcons.weather_windy),
                      SizedBox(height: 20.0),
                    ],
                  ),
                  Container(
                    height: _height * 0.7,
                    width: _width * 0.7,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50.0),
                        bottomLeft: Radius.circular(50.0),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: AppColor.gardenGreen.withOpacity(0.2),
                          offset: Offset(-10.0, 20.0),
                          blurRadius: 20.0,
                          spreadRadius: 5.0,
                        ),
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50.0),
                        bottomLeft: Radius.circular(50.0),
                      ),
                      child: Image(
                        image:
                            AssetImage(smallCards[widget.index].imageAddress),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: _width * 0.3 / 4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          smallCards[widget.index].plantName,
                          textScaleFactor: 0.85,
                          style: TextStyle(
                            color: AppColor.heading,
                            fontSize: 40.0,
                            fontFamily: 'HK Grotesk',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Text(
                          smallCards[widget.index].cost,
                          textScaleFactor: 0.85,
                          style: TextStyle(
                            color: AppColor.gardenGreen,
                            fontSize: 35.0,
                            fontFamily: 'HK Grotesk',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: _width * 0.3 / 4),
                      child: Text(
                        smallCards[widget.index].plantType,
                        textScaleFactor: 0.85,
                        style: TextStyle(
                          color: AppColor.gardenGreen,
                          fontSize: 25.0,
                          fontFamily: 'HK Grotesk',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      height: (_height * 0.3 / 2) - 20,
                      decoration: BoxDecoration(
                        color: AppColor.gardenGreen,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30.0),
                        ),
                      ),
                      child: Text(
                        'Buy Now',
                        textScaleFactor: 0.85,
                        style: TextStyle(
                          color: AppColor.lessWhite,
                          fontSize: 20.0,
                          fontFamily: 'HK Grotesk',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      height: (_height * 0.3 / 2) - 20,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10.0),
                        ),
                      ),
                      child: Text(
                        'Description',
                        textScaleFactor: 0.85,
                        style: TextStyle(
                          color: AppColor.heading,
                          fontSize: 20.0,
                          fontFamily: 'HK Grotesk',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        //* background END >>>------------------>
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: _width * 0.3 / 4, vertical: 20.0),
          child: Material(
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(Feather.arrow_left),
                  iconSize: 30.0,
                  color: AppColor.heading,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                IconButton(
                  icon: Icon(Feather.more_horizontal),
                  iconSize: 30.0,
                  color: AppColor.lessWhite,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
