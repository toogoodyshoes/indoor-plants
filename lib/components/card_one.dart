import 'package:flutter/material.dart';
import 'package:indoor_plants/configs/colors.dart';
import 'package:indoor_plants/configs/constants.dart';
import 'package:indoor_plants/screens/product_page.dart';

class SmallCard extends StatelessWidget {
  final int index;

  SmallCard({this.index});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 10.0,
      ),
      child: GestureDetector(
        child: Container(
          height: 210.0,
          width: 130.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  topRight: Radius.circular(10.0),
                ),
                child: Container(
                  height: 160,
                  child: Image.asset(smallCards[index].imageAddress,
                      fit: BoxFit.cover),
                ),
              ),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                decoration: BoxDecoration(
                  color: AppColor.lessWhite,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: AppColor.gardenGreen.withOpacity(0.1),
                      offset: Offset(0.0, 20.0),
                      blurRadius: 20.0,
                      spreadRadius: 5.0,
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          smallCards[index].plantName.toUpperCase(),
                          textScaleFactor: 0.85,
                          style: TextStyle(
                              color: AppColor.heading,
                              fontSize: 15.0,
                              fontFamily: 'Product Sans'),
                        ),
                        Text(
                          smallCards[index].cost,
                          textScaleFactor: 0.85,
                          style: TextStyle(
                              color: AppColor.gardenGreen,
                              fontSize: 15.0,
                              fontFamily: 'Product Sans'),
                        ),
                      ],
                    ),
                    SizedBox(height: 5.0),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        smallCards[index].plantType.toUpperCase(),
                        textScaleFactor: 0.85,
                        style: TextStyle(
                            color: AppColor.gardenGreen,
                            fontSize: 10.0,
                            fontFamily: 'Product Sans'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => ProductPage(index: index),
            ),
          );
        },
      ),
    );
  }
}
