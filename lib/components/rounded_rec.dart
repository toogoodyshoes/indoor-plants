import 'package:flutter/material.dart';
import 'package:indoor_plants/configs/colors.dart';

class PlantInfoBox extends StatelessWidget {
  final IconData icon;
  PlantInfoBox({this.icon});

  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: _width * 0.3 / 4, vertical: 15.0,),
      child: Container(
        width: _width * 0.3 / 2,
        height: _width * 0.3 / 2,
        decoration: BoxDecoration(
          color: AppColor.lessWhite,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: AppColor.gardenGreen.withOpacity(0.2),
              offset: Offset(5.0, 20.0),
              blurRadius: 10.0,
            ),
            // BoxShadow(
            //   color: Colors.black.withOpacity(0.2),
            //   offset: Offset(5.0, 5.0),
            //   blurRadius: 10.0,
            //   spreadRadius: -5.0,
            // ),
          ],
        ),
        child: Icon(
          icon,
          color: AppColor.gardenGreen,
          size: 30.0,
        ),
      ),
    );
  }
}
