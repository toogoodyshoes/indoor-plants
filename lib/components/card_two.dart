import 'package:flutter/material.dart';
import 'package:indoor_plants/configs/colors.dart';
import 'package:indoor_plants/configs/constants.dart';

class BigCard extends StatelessWidget {
  final int index;

  BigCard({this.index});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 10.0,
      ),
      child: Container(
        height: 400.0,
        width: 300.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
              ),
              child: Container(
                height: 280,
                child: Image(
                    image: AssetImage(bigCards[index].imageAddress),
                    fit: BoxFit.cover),
              ),
            ),
            Container(
              height: 70,
              padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
              decoration: BoxDecoration(
                color: AppColor.lessWhite,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: AppColor.gardenGreen.withOpacity(0.1),
                    offset: Offset(0.0, 20.0),
                    blurRadius: 20.0,
                    spreadRadius: 5.0,
                  ),
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        bigCards[index].plantName.toUpperCase(),
                        textScaleFactor: 0.85,
                        style: TextStyle(
                            color: AppColor.heading,
                            fontSize: 25.0,
                            fontFamily: 'HK Grotesk',
                            fontWeight: FontWeight.w600,),
                      ),
                      Text(
                        bigCards[index].cost,
                        textScaleFactor: 0.85,
                        style: TextStyle(
                            color: AppColor.gardenGreen,
                            fontSize: 25.0,
                            fontFamily: 'Sen'),
                      ),
                    ],
                  ),
                  //SizedBox(height: 5.0),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      bigCards[index].plantType.toUpperCase(),
                      textScaleFactor: 0.85,
                      style: TextStyle(
                          color: AppColor.gardenGreen,
                          fontSize: 15.0,
                          fontFamily: 'Sen'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
