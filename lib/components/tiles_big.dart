import 'package:flutter/material.dart';
import 'package:indoor_plants/components/card_one.dart';
import 'package:indoor_plants/components/card_two.dart';
import 'package:indoor_plants/configs/colors.dart';
import 'package:indoor_plants/configs/constants.dart';

class CarouselTwo extends StatefulWidget {
  @override
  _CarouselTwoState createState() => _CarouselTwoState();
}

class _CarouselTwoState extends State<CarouselTwo> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Container(
        //color: Colors.red,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            //* Heading >>>---------------------------------------------->
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Featured Plants',
                    textScaleFactor: 0.85,
                    style: TextStyle(
                      color: AppColor.heading,
                      fontFamily: 'HK Grotesk',
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                    decoration: BoxDecoration(
                      color: AppColor.gardenGreen,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Text(
                      'More',
                      textScaleFactor: 0.85,
                      style: TextStyle(
                        color: AppColor.lessWhite,
                        fontFamily: 'HK Grotesk',
                        fontWeight: FontWeight.w600,
                        fontSize: 15.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //* Heading END >>>------------------------------------------>
            SizedBox(height: 10.0,),
            //* Carousel >>>--------------------------------------------->
            Container(
              height: 440.0,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                itemCount: bigCards.length,
                itemBuilder: (context, index) => BigCard(index: index),
              ),
            ),
            //* Carousel END >>>----------------------------------------->
          ],
        ),
      ),
    );
  }
}
