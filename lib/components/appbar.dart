import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:indoor_plants/configs/colors.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class GreenAppBar extends StatefulWidget {
  @override
  _GreenAppBarState createState() => _GreenAppBarState();
}

class _GreenAppBarState extends State<GreenAppBar> {
  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    return Material(
      color: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Container(
            height: _height * 0.35,
            width: double.infinity,
            decoration: BoxDecoration(
              color: AppColor.gardenGreen,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(50.0),
                bottomRight: Radius.circular(50.0),
              ),
            ),
          ),
          Container(
            height: _height * 0.35 + (45 / 2),
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(MdiIcons.sortVariant),
                    color: Colors.white,
                    iconSize: 30.0,
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    onPressed: () {
                      //TODO Show Drawbar
                    },
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Hi Steve!',
                          textScaleFactor: 0.85,
                          style: TextStyle(
                            color: AppColor.lessWhite,
                            fontSize: 30.0,
                            fontFamily: 'HK Grotesk',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/profile-pic.png'),
                          radius: 30.0,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    child: Container(
                      height: 45.0,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: AppColor.lessWhite,
                        borderRadius: BorderRadius.circular(15.0),
                        boxShadow: [
                          BoxShadow(
                            color: AppColor.gardenGreen.withOpacity(0.2),
                            offset: Offset(0.0, 10.0),
                            blurRadius: 20.0,
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Search',
                              textScaleFactor: 0.85,
                              style: TextStyle(
                                color: AppColor.gardenGreen.withOpacity(0.5),
                                fontSize: 18.0,
                                fontFamily: 'HK Grotesk',
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            Icon(
                              AntDesign.search1,
                              color: AppColor.gardenGreen.withOpacity(0.5),
                              size: 20.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
