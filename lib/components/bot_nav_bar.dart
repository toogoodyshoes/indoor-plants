import 'package:flutter/material.dart';
import 'package:indoor_plants/configs/colors.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class WhiteBotNavBar extends StatefulWidget {
  @override
  _WhiteBotNavBarState createState() => _WhiteBotNavBarState();
}

class _WhiteBotNavBarState extends State<WhiteBotNavBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
      decoration: BoxDecoration(
        color: AppColor.lessWhite,
        boxShadow: [
          BoxShadow(
            color: AppColor.gardenGreen.withOpacity(0.2),
            offset: Offset(0.0, -10.0),
            blurRadius: 20.0,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          NavBarIcon(icon: MdiIcons.star),
          NavBarIcon(icon: MdiIcons.heartOutline),
          NavBarIcon(icon: MdiIcons.accountOutline),
        ],
      ),
    );
  }
}

class NavBarIcon extends StatelessWidget {
  final IconData icon;

  NavBarIcon({this.icon});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
      child: Icon(
        icon,
        color: AppColor.gardenGreen,
        size: 30.0,
      ),
    );
  }
}
