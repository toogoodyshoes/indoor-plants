# Indoor Plants [![LICENSE](https://img.shields.io/badge/license-GNU%20GPLv3-blue)](LICENSE)

Simple 2 screen template.

## Built With

* [Flutter](https://flutter.dev)

## Authors
* **Nihar Lekhade** - *Developer* - [ghostEleven](https://gitlab.com/ghostEleven)

## Credits

Based on the [design](https://dribbble.com/shots/11533153-Daily-UI-Challenge-069-100-Plant-Shop-App-Freebie) by [Ankur Tripathi](https://dribbble.com/Ankur_007)

## License

This project is licensed under GNU GPLv3. See [LICENSE](LICENSE) for more details.

## Screenshots

![App gif](Screenshots/indoor.gif)